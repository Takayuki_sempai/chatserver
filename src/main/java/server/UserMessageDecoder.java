package server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.nio.charset.Charset;
import java.util.List;

public class UserMessageDecoder extends ByteToMessageDecoder {

    private final Charset charset;

    /**
     * Creates a new instance with the current system character set.
     */
    public UserMessageDecoder() {
        this(Charset.defaultCharset());
    }

    /**
     * Creates a new instance with the specified character set.
     */
    public UserMessageDecoder(Charset charset) {
        if (charset == null) {
            throw new NullPointerException("charset");
        }
        this.charset = charset;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        System.err.println(msg.readBytes(msg.readableBytes()).toString(charset));
        System.err.println("Next request");
        if(internalBuffer().readableBytes() < 8) {
            return;
        }

        int nameLength = msg.readInt();
        int messageLength = msg.readInt();
        if(internalBuffer().readableBytes() < nameLength + messageLength) {
            internalBuffer().resetReaderIndex();
            return;
        }

        String name = msg.readBytes(nameLength).toString(charset);
        String message = msg.readBytes(internalBuffer().readableBytes()).toString(charset);
        out.add(new UserMessage(name, message));
    }
}
