package server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.nio.charset.Charset;

public class UserMessageEncoder extends MessageToByteEncoder<UserMessage> {

    private final Charset charset;

    /**
     * Creates a new instance with the current system character set.
     */
    public UserMessageEncoder() {
        this(Charset.defaultCharset());
    }

    /**
     * Creates a new instance with the specified character set.
     */
    public UserMessageEncoder(Charset charset) {
        if (charset == null) {
            throw new NullPointerException("charset");
        }
        this.charset = charset;
    }

//    @Override
//    protected void encode(ChannelHandlerContext ctx, UserMessage msg, List<Object> out) throws Exception {
//
//    }

    @Override
    protected void encode(ChannelHandlerContext ctx, UserMessage msg, ByteBuf out) throws Exception {
        out.writeInt(msg.getName().getBytes(charset).length);
        out.writeInt(msg.getMessage().getBytes(charset).length);
        out.writeCharSequence(msg.getName(), charset);
        out.writeCharSequence(msg.getMessage(), charset);
        out.writeCharSequence("\n", charset);
    }
}
